#!/bin/bash

# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# check if root

[[ $EUID -ne 0 ]] && { echo "Must be run as root"; exit 1; }

cp -v zabbix_agentd.d/ossec-alert.conf /etc/zabbix/zabbix_agentd.conf.d/
cp -v ossec-alert /usr/local/bin
chmod -v +x /usr/local/bin/ossec-alert

echo "In Zabbix create a new Item with Key OSSEC-alert"
