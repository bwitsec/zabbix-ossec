# zabbix-ossec

Alert script from [zabbix-alert](https://github.com/didier13150/rpm/blob/master/ossec-hids/zabbix-alert.sh)

Submits an OSSEC alert as a passive service check result to zabbix.

## Installation

Run `install.sh` as root, or the following command:

	cp -v zabbix_agentd.d/ossec-alert.conf /etc/zabbix/zabbix_agentd.conf.d/
	cp -v ossec-alert /usr/local/bin
	chmod -v +x /usr/local/bin/ossec-alert

In Zabbix create a new Item with Key OSSEC-alert.
